from django.http import HttpResponse,HttpResponsePermanentRedirect
from .models import myassignmentdb
from django.template import loader
from oauth2client import client, crypt
from django.views.decorators.csrf import csrf_exempt
import datetime
import json

def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render(request))

def home(request):
    try:
        token_id=request.GET['id']
        idinfo = client.verify_id_token(token_id, '201957125577-blqlfuqg0ea5eriq8sgl5ntsln82b2of.apps.googleusercontent.com')
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise crypt.AppIdentityError("Wrong issuer.")
        template = loader.get_template('home.html')
        all_data = myassignmentdb.objects.filter(updatedBy=idinfo['email'])
        context = {'all_data': all_data,
                   'uname' : idinfo['name'],
                   'uemail' : idinfo['email'],
                   'profilepic': idinfo['picture'],
                   'token_id': token_id,
        }
    except:
        return HttpResponsePermanentRedirect('index')
    return HttpResponse(template.render(context,request))

@csrf_exempt
def storeRec(request):
    try:
        data = request.POST['data']
        token_id = request.POST['id']
        idinfo = client.verify_id_token(token_id,'201957125577-blqlfuqg0ea5eriq8sgl5ntsln82b2of.apps.googleusercontent.com')
        chkdb=myassignmentdb.objects.filter(updatedBy=idinfo['email']).order_by('-updateID')
        if chkdb:
            updID=chkdb[0].updateID+1
        else:
            updID=1
        a = myassignmentdb(updateID=updID,textdata=data, updateTime=str(datetime.datetime.now()), updatedBy=str(idinfo['email']))
        a.save()
        resp = {}
        resp["id"] = str(a.updateID)
        resp["updateTime"] = a.updateTime
        json_resp = json.dumps(resp)
    except:
        return HttpResponsePermanentRedirect('index')
    return HttpResponse(json_resp)
