from django.db import models


class myassignmentdb(models.Model):
    updateID = models.IntegerField()
    textdata = models.CharField(max_length=400)
    updateTime = models.CharField(max_length=20)
    updatedBy= models.CharField(max_length=100)
    def __str__(self):
        return self.textdata + ' - ' + self.updateTime+' - '+self.updatedBy